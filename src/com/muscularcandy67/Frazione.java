package com.muscularcandy67;

public class Frazione {
    private int numeratore;
    private int denominatore;

    public Frazione (int n, int d){
        setNumeratore(n);
        setDenominatore(d);
    }
    public Frazione(){
        numeratore=0;
        denominatore = 1;
    }

    public void setNumeratore(int numeratore) {
        this.numeratore = numeratore;
    }

    public void setDenominatore(int denominatore) {
        this.denominatore = denominatore;
    }

    public int getNumeratore() {
        return numeratore;
    }

    public int getDenominatore() {
        return denominatore;
    }

    public void minimiTermini(){
        int l;
        if(numeratore<denominatore){
            l=numeratore;
        }
        else l = denominatore;
        int i;
        for(i=2; i<l; i++){
            if((numeratore%i)==0 && (denominatore%i)==0){
                numeratore/=i;
                denominatore/=i;
                if(numeratore<denominatore){
                    l=numeratore;
                }
                else l = denominatore;
                i=2;
            }
        }
    }
    public boolean checkMinimiTermini(){
        int l;
        if(numeratore<denominatore){
            l=numeratore;
        }
        else l = denominatore;
        int i;
        for(i=2; i<l; i++){
            if((numeratore%i)==0 && (denominatore%i)==0){
                return true;
            }
        }
        return false;
    }

    public double decimale(){
        return (double) numeratore/ (double) denominatore;
    }

    public void somma(Frazione frazione){
        int n1 = numeratore * frazione.getDenominatore();
        int n2 = frazione.getNumeratore() * denominatore;
        int d = denominatore * frazione.getDenominatore();
        numeratore = n1 + n2;
        denominatore = d;
        minimiTermini();
    }

    public void differenza(Frazione frazione){
        int n1 = numeratore * frazione.getDenominatore();
        int n2 = frazione.getNumeratore() * denominatore;
        int d = denominatore * frazione.getDenominatore();
        numeratore = n1 - n2;
        denominatore = d;
        minimiTermini();
    }

    public void moltiplicazione(Frazione frazione){
        numeratore*= frazione.getNumeratore();
        denominatore*= frazione.getDenominatore();
        minimiTermini();
    }

    public void divisione(Frazione frazione){
        numeratore*= frazione.getDenominatore();
        denominatore*= frazione.getNumeratore();
        minimiTermini();
    }

    public String checkTipo(){
        if(numeratore==denominatore){
            return "Apparente";
        }
        if(numeratore>denominatore){
            return "Impropria";
        }
        return "Propria";
    }

    public String isEquals(Frazione frazione){
        if(decimale()>frazione.decimale()){
            return "La prima frazione è maggiore";
        }
        if(decimale()==frazione.decimale()){
            return "Le frazioni sono uguali";
        }
        return "La seconda frazione è maggiore";
    }

    public String toString(){
        return numeratore + "/" + denominatore;
    }
}
