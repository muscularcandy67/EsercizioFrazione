package com.muscularcandy67;

public class Main {

    public static void main(String[] args) {
        Frazione f1 = new Frazione(9,21);
        Frazione f2 = new Frazione(2,3);
        f1.somma(f2);
        System.out.println(f1.toString());
        f1 = new Frazione(9,21);
        f1.differenza(f2);
        System.out.println(f1.toString());
        f1 = new Frazione(9,21);
        f1.moltiplicazione(f2);
        System.out.println(f1.toString());
        f1 = new Frazione(9,21);
        f1.divisione(f2);
        System.out.println(f1.toString());
        f1 = new Frazione(9,21);
        System.out.println(f1.isEquals(f2));
        System.out.println(f1.checkMinimiTermini());
        f1.minimiTermini();
        System.out.println(f1.toString());
        System.out.println(f1.checkTipo());
    }
}
